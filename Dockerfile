FROM ruby:2.1

RUN git clone git@gitlab.com:nomanod/docker-gitlab.git /app

WORKDIR /app

RUN bundle install

RUN bundle exec rake db:create
RUN bundle exec rake db:migrate

ENTRYPOINT "bundle exec rails server"
